# Bravoure - Block List Usp

## Directive: Code to be added:

        <block-list-usp></block-list-usp>


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-block-list-usp": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
