(function () {
    'use strict';

    function blockListUsp () {
        return {
            restrict: 'E',
            replace: false,

            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                // defines the type of list
                $scope.module.kind = 'usp';
            },

            template: '<block-list></block-list>'
        }
    }

    blockListUsp.$inject = ['PATH_CONFIG'];

    angular
        .module('bravoureAngularApp')
        .directive('blockListUsp', blockListUsp);

})();
